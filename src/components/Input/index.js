import React from 'react';
import Autocomplete from 'react-autocomplete';

import './styles.css';

const Input = (props) => {
    const {label, autoComplete, placeholder, isValid = true} = props;

    return(
        <>
            {label && <span className='label'>{label}</span>}
            {autoComplete
                ?
                <Autocomplete
                    {...props}
                    inputProps={{
                        className: isValid ? 'autoCompleteInput' : 'input invalid',
                        placeholder
                    }}
                    wrapperStyle={{display: 'flex'}}
                />
                :
                <input
                    {...props}
                    className={isValid ? 'input' : 'input invalid'}
                />
            }
        </>
    )
};

export default Input;

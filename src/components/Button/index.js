import React from 'react';

import './styles.css';

const Button = (props) => {
    const {text, primary} = props;

    return(
        <button className={primary ? 'buttonPrimary' : 'button'} {...props}>
            {text}
        </button>
    )
};

export default Button;

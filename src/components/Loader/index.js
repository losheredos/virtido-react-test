import React from 'react';

import './styles.css';

const Loader = () => {
    return(
        <span className="loading-dots-typing">
            <span className="dot1" />
            <span className="dot2" />
            <span className="dot3" />
	    </span>
    )
};

export default Loader;

import axios from 'axios';

export function getCities(rows = 35) {
    // We could save the response-data in localStorage to not call api again but depends on the requirement

    return axios.get('https://swisspost.opendatasoft.com/api/records/1.0/search/',{
        params:{
            dataset: "plz_verzeichnis_v2",
            ortbez: 18,
            rows
        }
    })
}


export function getPremiumOffer() {
    return axios.get('https://5f2ef02e64699b00160293e7.mockapi.io/premium/1');
}

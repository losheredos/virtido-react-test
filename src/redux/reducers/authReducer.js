import {$} from '../actions';

const initialState = {
    postCodes: ['3000', '8000', '6000'],
    cities: [
        {label: '3000: Bern', address: 'Bern', postCode: '3000'},
        {label: '8000: Zurich', address: 'Zurich', postCode: '8000'},
        {label: '6000: Luzern', address: 'Luzern', postCode: '6000'}
    ],
    citiesLoading: true,
    offerLoading: true,
    premiumValue: null
};

const authReducer = (state = initialState, action) => {
    switch(action.type){
        case $.GET_CITIES_SUCCESS:{
            return {...state, postCodes: action.postCodes, cities: action.cities, citiesLoading: false}
        }
        case $.GET_PREMIUM_OFFER_SUCCESS:{
            return {...state, premiumValue: action.payload}
        }
        default: return state;
    }
};

export default authReducer;

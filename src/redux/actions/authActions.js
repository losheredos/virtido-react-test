import {$} from "./index";

export function getCities() {
    return{
        type: $.GET_CITIES
    }
}

export function getCitiesSuccess(postCodes, cities) {
    return{
        type: $.GET_CITIES_SUCCESS,
        postCodes,
        cities
    }
}

export function getPremiumOffer() {
    return{
        type: $.GET_PREMIUM_OFFER
    }
}

export function getPremiumOfferSuccess(premiumValue) {
    return{
        type: $.GET_PREMIUM_OFFER_SUCCESS,
        payload: premiumValue
    }
}

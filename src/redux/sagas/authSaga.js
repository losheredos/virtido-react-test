import {call, delay, put, takeLatest} from "@redux-saga/core/effects";
import {$, authActions} from '../actions';
import * as apis from '../../api';

function* getCities() {
    try{
        const result = yield call(apis.getCities);
        const postCodes = new Array(0), cities = new Array(0);

        result.data.records.map(data => {
            const postCode = data.fields.postleitzahl.toString();
            const address = data.fields.ortbez18;
            postCodes.push(postCode);
            cities.push({
                label: postCode+': '+address,
                address: address,
                postCode
            })
        });
        yield put(authActions.getCitiesSuccess(postCodes, cities))
    }
    catch (e) {
        console.warn(e)
    }
}

function* getPremiumOffer() {
    try{
        const result = yield call(apis.getPremiumOffer);
        let offer = result.data.premium;
        // Some randomness which would be normally like this depending on the values we would send with API
        offer += Math.floor(Math.random() * 100) + 1;
        yield delay(750);
        yield put(authActions.getPremiumOfferSuccess(offer))
    }
    catch (e) {
        console.warn(e)
    }
}

const authSaga = [
    takeLatest($.GET_CITIES, getCities),
    takeLatest($.GET_PREMIUM_OFFER, getPremiumOffer)
];

export default authSaga;

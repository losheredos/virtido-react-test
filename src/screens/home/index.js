import React, {useState, useEffect} from 'react';

import Fade from 'react-reveal/Fade';
import {useDispatch, useSelector} from "react-redux";

import {authActions} from "../../redux/actions";
import './styles.css';
import {Button, Input, Loader} from "../../components";
import {images} from '../../utils';

const Home = () => {
    const [view, setView] = useState(0);

    const [postCode, setPostCode] = useState('');
    const [postCodeValid, setPostCodeValid] = useState(true);

    const [address, setAddress] = useState('');
    const [addressValid, setAddressValid] = useState(true);
    const [isAutoVisible, setIsAutoVisible] = useState(false);

    const [peopleAmount, setPeopleAmount] = useState(0);
    const [ageValid, setAgeValid] = useState(true);
    const [age, setAge] = useState(0);

    const [nextText, setNextText] = useState('Next');

    const dispatch = useDispatch();
    const postCodes = useSelector(state => state.authReducer.postCodes);
    const cities = useSelector(state => state.authReducer.cities);
    const citiesLoading = useSelector(state => state.authReducer.citiesLoading);
    const premiumValue = useSelector(state => state.authReducer.premiumValue);

    useEffect(() => {
        dispatch(authActions.getCities())
    }, []);

    function onPostCodeChange(value) {
        let found = false;
        postCodes.map(postCode => {
            if(postCode.includes(value) && value.length > 0){
                found = true;
            }
        });
        setPostCode(value);
        setIsAutoVisible(found);
    }

    function getFilteredCities() {
        let filteredCities = cities.filter(city => {
            return city.postCode.toLowerCase().indexOf(postCode) != -1;
        });
        return filteredCities
    }

    function onAutoSelectSelected(item) {
        setAddressValid(true);
        setPostCodeValid(true);
        setIsAutoVisible(false);
        setPostCode(item.postCode);
        setAddress(item.address);
    }

    function renderViews() {
        if(view === 0){
            return(
                <>
                    <Input
                        isValid={postCodeValid}
                        label={"Post Code"}
                        value={postCode}
                        placeholder="Enter your post code"
                        onChange={e => onPostCodeChange(e.target.value) + setPostCodeValid(true)}
                    />
                    <Input
                        isValid={addressValid}
                        label={"Address"+ (citiesLoading ? ' ...' : '')}
                        autoComplete={true}
                        open={isAutoVisible}
                        placeholder="Enter your address"
                        getItemValue={(item) => item.label}
                        items={getFilteredCities()}
                        renderItem={(item, isHighlighted) =>
                            <div key={item.label} style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                                {item.label}
                            </div>
                        }
                        value={address}
                        onChange={(e) => setAddress(e.target.value) + setAddressValid(true)}
                        onSelect={(val, item) => onAutoSelectSelected(item)}
                    />
                </>
            )
        }
        if(view === 1){
            return(
                <Fade clear>
                    <div className="imageContainer">
                        <img
                            alt={'One Person'}
                            onClick={() => setPeopleAmount(1)}
                            src={images.icons.person}
                            className={peopleAmount === 1 ? "selectedImage" : "image"}
                        />
                        <img
                            alt={'Many People'}
                            onClick={() => setPeopleAmount(2)}
                            src={images.icons.people}
                            className={peopleAmount === 2 ? "selectedImage" : "image"}
                        />
                    </div>
                </Fade>
            )
        }
        if(view === 2){
            return(
                <Input
                    isValid={ageValid}
                    value={age === 0 ? '' : age}
                    min="18"
                    max="80"
                    type="number"
                    onChange={e => {
                        let val = e.target.value;
                        if(val.length <= 2){
                            setAge(val);
                            setAgeValid(true);
                        }
                    }}
                    label="Your age (years)"
                />
            )
        }
        if(view === 3){
            return(
                <div className="offerContainer">
                    {premiumValue
                        ?
                        <b>Your premium is {premiumValue}€</b>
                        :
                        <div className="centeredContainer">
                            <Loader/>
                            Your offer is loading
                        </div>
                    }
                </div>
            )
        }
    }

    function proceedToNext() {
        if(view === 0){
            let failed = false;
            if(postCode.trim().length === 0){
                failed = true;
                setPostCodeValid(false);
            }
            if(address.trim().length === 0){
                failed = true;
                setAddressValid(false);
            }

            if(failed){
                return warnForValidation();
            }
            setView(view+1);
        }
        if(view === 1){
            if(peopleAmount === 0){
                return warnForValidation();
            }
            setNextText('Get Premium Offer');
            setView(view+1);
        }
        if(view === 2){
            if(age < 18 || age.length === 0 || age === 0){
                setAgeValid(false);
                return warnForValidation();
            }
            dispatch(authActions.getPremiumOffer());
            setView(view+1)
        }
    }

    function proceedBack() {
        setNextText('Next');
        setView(view === 0 ? 0 : view - 1);
    }

    function warnForValidation() {
        let warningText;
        switch (view) {
            case 0: warningText = 'You need to fill all the fields to proceed'; break;
            case 1: warningText = 'You need choose who will be insured'; break;
            case 2: warningText = 'You should type your age (min 18)'; break;
        }
        setNextText(warningText);
        let nextText = view === 2 ? 'Get Premium Offer' : 'Next';
        setTimeout(() => setNextText(nextText), 2000);
    }

    function renderTitles() {
        switch (view) {
            case 0: return 'Where do you live?';
            case 1: return 'Who will be insured?';
            case 2: return 'How old are you?';
            case 3: return 'Offer';
        }
    }

    return(
        <Fade clear>
            <div className="container">
                <span className="title">{renderTitles()}</span>
                <div className="viewContainer">
                    {renderViews()}
                </div>
                <div className={(view === 0) ? "singleButtonContainer" : "doubleButtonContainer"}>
                    {view > 0 && <Button text={"Back"} onClick={() => proceedBack()}/>}
                    {view < 3 && <Button text={nextText} onClick={() => proceedToNext()} primary={true} type="submit" />}
                </div>
            </div>
        </Fade>
    )
};

export default Home;

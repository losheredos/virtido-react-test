const colors = {
    pink: '#fe7273',
    black: '#333',

    lightBlue: '#d0e3f4',
    blue: '#005ba1',

    yellow: '#ffc000'
};

export default colors;

const images = {
    icons:{
        person: require('./icons/person.png'),
        people: require('./icons/people.png')
    }
};

export default images;
